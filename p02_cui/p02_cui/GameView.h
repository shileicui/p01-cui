//
//  GameView.h
//  p02_cui
//
//  Created by SHILEI CUI on 2/7/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GameView;

@protocol GameViewDelegate <NSObject>

@optional

- (void)gameView:(GameView *)gameView withSwipeGesture:(UISwipeGestureRecognizer *)gesture;


@end


@interface GameView : UIView

@property (nonatomic, weak) id<GameViewDelegate> delegate;

- (void)updataGameViewWithData:(NSMutableArray *)date;

@end
