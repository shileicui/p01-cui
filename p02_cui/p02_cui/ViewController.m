//
//  ViewController.m
//  p02_cui
//
//  Created by SHILEI CUI on 2/3/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "ViewController.h"
#import "Color.h"
#import "ScoreView.h"
#import "GameView.h"
#import "GameModel.h"

@interface ViewController () <GameViewDelegate, DataDelegate>

@property (weak, nonatomic) IBOutlet UIButton *restartButton;

@property (weak, nonatomic) IBOutlet ScoreView *scoreView;
@property (weak, nonatomic) IBOutlet ScoreView *bestView;

@property (strong, nonatomic) GameView *gameView;
@property (strong, nonatomic) GameModel *dataManager;

- (IBAction)restartClick;

@end

@implementation ViewController


- (GameView *)gameView
{
    if (!_gameView) {
        _gameView = [[GameView alloc] init];
        [self.view addSubview:_gameView];
        _gameView.delegate = self;
    }
    
    return _gameView;
}

-(GameModel *)dataManager
{
    if (!_dataManager) {
        _dataManager = [[GameModel alloc] init];
        _dataManager.delegate = self;
    }
    
    return _dataManager;
}

- (void)updateAll
{
    [self.scoreView updateWithScore:self.dataManager.currentScore];
    [self.bestView updateWithScore:self.dataManager.bestScore];
    [self.gameView updataGameViewWithData:self.dataManager.dataArray];
}

- (IBAction)restartClick {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are You Sure To Restart？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.dataManager restart];
        [self updateAll];
    }]];
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [Color backgroundColor];
    [self.scoreView updateAppearance];
    [self.bestView updateAppearance];
    self.restartButton.layer.cornerRadius = 6;
    self.restartButton.layer.masksToBounds = YES;
    self.restartButton.backgroundColor = [Color buttonColor];
    [self updateAll];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - GameViewDelegateMethod
- (void)gameView:(GameView *)gameView withSwipeGesture:(UISwipeGestureRecognizer *)gesture
{
    switch (gesture.direction) {
        case UISwipeGestureRecognizerDirectionUp:
            [self.dataManager swipeUp];
            break;
        case UISwipeGestureRecognizerDirectionDown:
            [self.dataManager swipeDown];
            break;
        case UISwipeGestureRecognizerDirectionLeft:
            [self.dataManager swipeLeft];
            break;
        case UISwipeGestureRecognizerDirectionRight:
            [self.dataManager swipeRight];
            break;
            
        default:
            break;
    }
    [self updateAll];
}

#pragma mark - DataDelegateMethod
- (void)data:(GameModel *)data
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are You Sure To Restart？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.dataManager restart];
        [self updateAll];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
}

@end
