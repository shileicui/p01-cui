//
//  main.m
//  p03_cui
//
//  Created by SHILEI CUI on 2/12/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
