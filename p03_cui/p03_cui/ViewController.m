//
//  ViewController.m
//  p03_cui
//
//  Created by SHILEI CUI on 2/12/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSTimer *movement;
}
@property (weak, nonatomic) IBOutlet UIImageView *brick5;
@property (weak, nonatomic) IBOutlet UIImageView *Jumper;
@property (weak, nonatomic) IBOutlet UIImageView *brick1;
@property (weak, nonatomic) IBOutlet UIImageView *brick2;
@property (weak, nonatomic) IBOutlet UIImageView *brick3;
@property (weak, nonatomic) IBOutlet UIImageView *brick4;
@property (weak, nonatomic) IBOutlet UIButton *startGame;
@property (weak, nonatomic) IBOutlet UILabel *GameOver;
@property (weak, nonatomic) IBOutlet UIButton *exitGame;
@property (weak, nonatomic) IBOutlet UILabel *finalScore;
@property (weak, nonatomic) IBOutlet UILabel *currentScore;

@end

@implementation ViewController

-(CGFloat)screenHeight{
    return [UIScreen mainScreen].bounds.size.height;
}

-(CGFloat)screenWidth{
    return [UIScreen mainScreen].bounds.size.width;
}

float brickMoveDown;

-(void)screenMoveDown{
    if(_Jumper.center.y > self.screenHeight*5/6){
        brickMoveDown = 1;
    }else if(_Jumper.center.y > self.screenHeight*4/6){
        brickMoveDown = 2;
    }else if(_Jumper.center.y > self.screenHeight*3/6){
        brickMoveDown = 4;
    }else if(_Jumper.center.y > self.screenHeight*2/6){
        brickMoveDown = 5;
    }else if(_Jumper.center.y >self.screenHeight*1/6){
        brickMoveDown = 6;
    }
}

BOOL jumperLeft;
BOOL jumperRight;
BOOL stopSideMove;
float sideMove;

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    jumperLeft = NO;
    jumperRight = NO;
    stopSideMove = YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    if(point.x < self.screenWidth/2){
        jumperLeft = YES;
    }else{
        jumperRight = YES;
    }
}

int brick3SideMove;
int brick5SideMove;
float brickMoveDown;

int randomPosition;

BOOL brick1WasUsed;
BOOL brick2WasUsed;
BOOL brick3WasUsed;
BOOL brick4WasUsed;
BOOL brick5WasUsed;

-(void)brickMovement{
    
    brickMoveDown -= 0.1;
    
    if(brickMoveDown < 0){
        brickMoveDown = 0;
    }
    
    _brick1.center = CGPointMake(_brick1.center.x, _brick1.center.y + brickMoveDown);
    _brick2.center = CGPointMake(_brick2.center.x, _brick2.center.y + brickMoveDown);
    _brick3.center = CGPointMake(_brick3.center.x + brick3SideMove, _brick3.center.y+brickMoveDown);
    _brick4.center = CGPointMake(_brick4.center.x, _brick4.center.y + brickMoveDown);
    _brick5.center = CGPointMake(_brick5.center.x + brick5SideMove, _brick5.center.y+brickMoveDown);
    
    CGFloat brickWidth = _brick1.frame.size.width;
    CGFloat brickHeight = _brick1.frame.size.height;
    
    if(_brick3.center.x < brickWidth/2){
        brick3SideMove = 2;
    }
    if(_brick3.center.x > self.screenWidth-brickWidth/2){
        brick3SideMove = -2;
    }
    
    if(_brick5.center.x < brickWidth/2){
        brick5SideMove = 2;
    }
    
    if(_brick5.center.x > self.screenWidth-brickWidth/2){
        brick5SideMove = -2;
    }
    
    int brickWidthBounds = self.screenWidth-brickWidth/2;
    
    if(_brick1.center.y > self.screenHeight-brickHeight/2){
        randomPosition = arc4random() % brickWidthBounds;
        randomPosition += (brickWidth/2);
        _brick1.center = CGPointMake(randomPosition, -brickHeight/2);
        brick1WasUsed = NO;
    }
    if(_brick2.center.y > self.screenHeight-brickHeight/2){
        randomPosition = arc4random() % brickWidthBounds;
        randomPosition += (brickWidth/2);
        _brick2.center = CGPointMake(randomPosition, -brickHeight/2);
        brick2WasUsed = NO;
    }
    if(_brick3.center.y > self.screenHeight-brickHeight/2){
        randomPosition = arc4random() % brickWidthBounds;
        randomPosition += (brickWidth/2);
        _brick3.center = CGPointMake(randomPosition, -brickHeight/2);
        brick3WasUsed = NO;
    }
    if(_brick4.center.y > self.screenHeight-brickHeight/2){
        randomPosition = arc4random() % brickWidthBounds;
        randomPosition += (brickWidth/2);
        _brick4.center = CGPointMake(randomPosition, -brickHeight/2);
        brick4WasUsed = NO;
    }
    if(_brick5.center.y > self.screenHeight-brickHeight/2){
        randomPosition = arc4random() % brickWidthBounds;
        randomPosition += (brickWidth/2);
        _brick5.center = CGPointMake(randomPosition, -brickHeight/2);
        brick5WasUsed = NO;
    }
    
}

int scoreNumber;
int highScoreNumber;
int addedScore;


-(void)gameOver{
    _GameOver.hidden = NO;
    _exitGame.hidden = NO;
    _finalScore.hidden = NO;
    _brick1.hidden = YES;
    _brick2.hidden = YES;
    _brick3.hidden = YES;
    _brick4.hidden = YES;
    _brick5.hidden = YES;
    _finalScore.text = [NSString stringWithFormat:@"Final Score is:%i",scoreNumber];
    [movement invalidate];
    
    if(scoreNumber > highScoreNumber){
        highScoreNumber = scoreNumber;
        [[NSUserDefaults standardUserDefaults]setInteger:highScoreNumber forKey:@"highScoreNumber"];
    }
}

-(void)scoring{
    scoreNumber = scoreNumber + addedScore;
    addedScore -= 1;
    if(addedScore < 0){
        addedScore = 0;
    }
    _currentScore.text = [NSString stringWithFormat:@"Score:%i",scoreNumber];

}

float upMove;
float sideMove;

-(void)jumperBounceAnimation{
    _Jumper.animationImages = [NSArray arrayWithObjects:
                             [UIImage imageNamed:@"Jumper1.png"],
                             [UIImage imageNamed:@"Jumper.png"],nil];
    [_Jumper setAnimationRepeatCount:1];
    _Jumper.animationDuration = 0.5;
    [_Jumper startAnimating];
    
    if(_Jumper.center.y > self.screenHeight*0.75){
        upMove = 6;
    }else if(_Jumper.center.y > self.screenHeight*0.5){
        upMove = 5;
    }else if(_Jumper.center.y > self.screenHeight*0.25){
        upMove = 4;
    }
}


-(void)jumperMovement{
    if(_Jumper.center.y > self.screenHeight){
        [self gameOver];
    }
    
    [self scoring];
    
    if(_Jumper.center.y < 250){
        _Jumper.center = CGPointMake(_Jumper.center.x, 250);
    }
    
    [self brickMovement];
    
    _Jumper.center = CGPointMake(_Jumper.center.x + sideMove, _Jumper.center.y - upMove);
    
    if(CGRectIntersectsRect(_Jumper.frame, _brick1.frame) && upMove < -2){
        [self jumperBounceAnimation];
        [self screenMoveDown];
        if(brick1WasUsed == NO){
            addedScore = 10;
            brick1WasUsed = YES;
        }
    }
    if (CGRectIntersectsRect(_Jumper.frame, _brick2.frame) && upMove < -2){
        [self jumperBounceAnimation];
        [self screenMoveDown];
        if(brick2WasUsed == NO){
            addedScore = 10;
            brick2WasUsed = YES;
        }
    }
    if(CGRectIntersectsRect(_Jumper.frame, _brick3.frame) && upMove < -2){
        [self jumperBounceAnimation];
        [self screenMoveDown];
        if(brick3WasUsed == NO){
            addedScore = 10;
            brick3WasUsed = YES;
        }
    }
    if(CGRectIntersectsRect(_Jumper.frame, _brick4.frame) && upMove < -2){
        [self jumperBounceAnimation];
        [self screenMoveDown];
        if(brick4WasUsed == NO){
            addedScore = 10;
            brick4WasUsed = YES;
        }
    }
    if(CGRectIntersectsRect(_Jumper.frame, _brick5.frame) && upMove < -2){
        [self jumperBounceAnimation];
        [self screenMoveDown];
        if(brick5WasUsed == NO){
            addedScore = 10;
            brick5WasUsed = YES;
        }
    }
    
    upMove -= 0.1;
    
    if(jumperLeft == YES){
        sideMove -= 0.3;
        if(sideMove < -5){
            sideMove = -5;
        }
    }
    if(jumperRight == YES){
        sideMove += 0.3;
        if(sideMove > 5){
            sideMove = 5;
        }
    }
    
    if(stopSideMove == YES && sideMove > 0){
        sideMove -= 0.1;
        if(sideMove < 0){
            sideMove = 0;
            stopSideMove = NO;
        }
    }
    if(stopSideMove == YES && sideMove < 0){
        sideMove += 0.1;
        if(sideMove > 0){
            sideMove = 0;
            stopSideMove = NO;
        }
    }
    
    
    CGFloat jumperWidth = _Jumper.frame.size.width;
    if(_Jumper.center.x < -jumperWidth/2){
        _Jumper.center = CGPointMake(self.screenWidth+jumperWidth/2, _Jumper.center.y);
    }
    if(_Jumper.center.x > self.screenWidth+jumperWidth/2){
        _Jumper.center = CGPointMake(-jumperWidth/2, _Jumper.center.y);
    }


    
}

- (IBAction)startGameClicked:(id)sender {
    //_exitGame.hidden = YES;
    _startGame.hidden = YES;
    upMove = -5;
    
    movement = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(jumperMovement) userInfo:nil repeats:YES];
    
    _brick2.hidden = NO;
    _brick3.hidden = NO;
    _brick4.hidden = NO;
    _brick5.hidden = NO;
    
    CGFloat brickWidth = _brick2.frame.size.width;
    int brickWidthBounds = self.screenWidth-brickWidth/2;
    
    randomPosition = arc4random() % brickWidthBounds;
    randomPosition += (brickWidth/2);
    _brick2.center = CGPointMake(randomPosition, self.screenHeight*4/5);
    
    randomPosition = arc4random() % brickWidthBounds;
    randomPosition += (brickWidth/2);
    _brick3.center = CGPointMake(randomPosition, self.screenHeight*3/5);
    
    randomPosition = arc4random() % brickWidthBounds;
    randomPosition += (brickWidth/2);
    _brick4.center = CGPointMake(randomPosition, self.screenHeight*2/5);
    
    randomPosition = arc4random() % brickWidthBounds;
    randomPosition += (brickWidth/2);
    _brick5.center = CGPointMake(randomPosition, self.screenHeight*1/5);
    
    brick3SideMove = -2;
    brick5SideMove = 2;
}

- (void)viewDidLoad {
    
    _brick2.hidden = YES;
    _brick3.hidden = YES;
    _brick4.hidden = YES;
    _brick5.hidden = YES;
    
    _GameOver.hidden = YES;
    _finalScore.hidden = YES;
    _exitGame.hidden = YES;
    
    scoreNumber = 0;
    addedScore = 0;
    
    brick1WasUsed = NO;
    brick2WasUsed = NO;
    brick3WasUsed = NO;
    brick4WasUsed = NO;
    brick5WasUsed = NO;
    
    upMove = 0;
    sideMove = 0;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
