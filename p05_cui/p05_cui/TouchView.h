//
//  TouchView.h
//  p05_cui
//
//  Created by SHILEI CUI on 3/26/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchView : UIView

- (instancetype)initWithFrame:(CGRect)frame rate:(float)rate;

- (void)growHeight;

- (void)endGrowAfter:(void(^)(float length))endGrowHandler;

- (void)dropAfter:(void (^)(float))endGrowHandler;

@end
