//
//  PlayerView.h
//  p05_cui
//
//  Created by SHILEI CUI on 3/26/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerView : UIView

- (void)run:(float)distance after:(dispatch_block_t)result;

- (void)dropAfter:(void(^)(void))endGrowHandler;

@end
