//
//  AppDelegate.h
//  p05_cui
//
//  Created by SHILEI CUI on 3/26/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>{

    AVAudioPlayer *audioPlayer;
}

@property (strong, nonatomic) UIWindow *window;


@end

