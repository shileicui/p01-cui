//
//  main.m
//  p01-cui
//
//  Created by SHILEI CUI on 1/17/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
