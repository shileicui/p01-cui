//
//  ViewController.m
//  p01-cui
//
//  Created by SHILEI CUI on 1/17/17.
//  Copyright © 2017 scui5. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloLabel;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)clicked:(id)sender
{
    [helloLabel setText:@"Shilei Cui completed the first assignment."];
    
    [helloLabel setTextColor:[UIColor purpleColor]];
    
    //add border to the label.
    [helloLabel.layer setMasksToBounds:YES];
    [helloLabel.layer setCornerRadius:8.0];
    [helloLabel.layer setBorderWidth:1.0];
}

@end
